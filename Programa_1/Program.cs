﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteliTrader
{
    class Program
    {
        static void Main(string[] args)
        {
            string mensagem = " ";
            char[] texto = new char[255];
            int contador = 0;

            do
            {
                Console.WriteLine("Digite uma mensagem(max de 255 caracteres): ");
                mensagem = Console.ReadLine();
            } while (mensagem.Length > 255);

            foreach (char x in mensagem)
            {
                texto[contador++] = x;
            }

            Sms mensagemTraduzida = new Sms();
            mensagemTraduzida.traduzSms(texto);

            Console.ReadKey();
        }
    }
}
