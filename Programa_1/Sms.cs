﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Sms
{
    Stack<int> parcial  = new Stack<int>();
    Queue<int> parcial2 = new Queue<int>();

    
    public void veri(int b)
    {
        int desem = parcial.Peek();

        if(parcial.Count > 0 && (desem == b || desem == b*11 || desem == b*111))
        {
            parcial.Push(1);
        }
    }

    public void substitui()
    {
        foreach(int b in parcial)
        {
            parcial2.Enqueue(b);
        }
        parcial.Clear();
        foreach(int b in parcial2)
        {
            parcial.Push(b);
        }
    }

    public void imprime()
    {
        parcial.Pop();
        foreach(int a in parcial)
        {
         if(a == 1)
            {
                Console.Write('_');
            }
            else
            {
                Console.Write(a);
            }
        }
    }

	public void traduzSms(char[] x)
	{
        parcial.Push(1);

        foreach(char a in x)
        {
            switch (a)
            {
                case 'a': veri(2); parcial.Push(2); break;
                case 'b': veri(2); parcial.Push(22); break;
                case 'c': veri(2); parcial.Push(222); break;

                case 'd': veri(3); parcial.Push(3); break;
                case 'e': veri(3); parcial.Push(33); break;
                case 'f': veri(3); parcial.Push(333); break;

                case 'g': veri(4); parcial.Push(4); break;
                case 'h': veri(4); parcial.Push(44); break;
                case 'i': veri(4); parcial.Push(444); break;

                case 'j': veri(5); parcial.Push(5); break;
                case 'k': veri(5); parcial.Push(55); break;
                case 'l': veri(5); parcial.Push(555); break;

                case 'm': veri(6); parcial.Push(6); break;
                case 'n': veri(6); parcial.Push(66); break;
                case 'o': veri(6); parcial.Push(666); break;

                case 'p': veri(7); parcial.Push(7); break;
                case 'q': veri(7); parcial.Push(77); break;
                case 'r': veri(7); parcial.Push(777); break;
                case 's': veri(7); parcial.Push(7777); break;

                case 't': veri(8); parcial.Push(8); break;
                case 'u': veri(8); parcial.Push(88); break;
                case 'v': veri(8); parcial.Push(888); break;

                case 'w': veri(9); parcial.Push(9); break;
                case 'x': veri(9); parcial.Push(99); break;
                case 'y': veri(9); parcial.Push(999); break;
                case 'z': veri(9); parcial.Push(9999); break;

                case ' ': veri(0); parcial.Push(0); break;
            }

        }

        substitui();
        imprime();
	}
}
